using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OpenMarket.Models
{
    public class CartItem
    {
        public int Id { get; set; }
        // public Order Order { get; set; }
        public IdentityUser Buyer { get; set; }
        // public OrderDetail OrderDetail { get; set; }

        public Product Product {get; set;}

        public int Quantity {get; set;}

        // [Required, Range(1, 1000000)]
        // public decimal CartTotalPrice { get; set; }
        // when doing a query you must use .Include(c => c.Product)... to fetch Product eagerly
        [NotMapped]
        public decimal PriceTotal { get {
            return Product.Price * Quantity;
        }}
    }
}