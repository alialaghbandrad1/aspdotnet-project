using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Identity;
// added
using Microsoft.AspNetCore.Http;

namespace OpenMarket.Models
{
    public partial class Product
    {
        
        public Product()
        {
            // OrderDetails = new HashSet<OrderDetails>();
            // Price = new HashSet<Price>();
            // ProductCategory = new HashSet<ProductCategory>();
            // Upload = new HashSet<Upload>();
        }
        
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        //public Category Category {get; set;}
        public int QuantityAvailable { get; set; }
        public bool? IsOnSale { get; set; }
        // public string CreatedByUserId { get; set; }
        public IdentityUser Seller { get; set; }
        public string PhotoPath { get; set; }
        public DateTime CreatedOnDate { get; set; }
        // public string LasUpdatedByUserId { get; set; }
        // public DateTime? LastUpdatedOnDate { get; set; }
        // public bool? IsDeleted { get; set; }
        public double Score {get; set;}
        public bool IsRecommended { get; set; }

        // public ICollection<OrderDetails> OrderDetails { get; set; }
        public decimal Price { get; set; }
        public decimal DiscountedPrice { get; set;}

        // public decimal Dictionary<string,decimal> PriceOptions { get; set;}
        public ProductCategory ProductCategory { get; set; }
        // public ICollection<ProductImage> Upload { get; set; }
        // public IFormFile UploadedFile { get; set; }
    }
}