using System;
using System.Collections.Generic;

namespace OpenMarket.Models
{
    public partial class ProductImage
    {
        public int Id { get; set; }
        public Product Product { get; set; }
        public string FilePath { get; set; }
        public bool IsThumbnail { get; set; }
        public bool IsCarousel { get; set; }
        public string CreatedByUserId { get; set; }
        public DateTime CreatedOnDate { get; set; }
        public string LasUpdatedByUserId { get; set; }
        public DateTime LastUpdatedOnDate { get; set; }
        public bool IsDeleted { get; set; }

    }
}