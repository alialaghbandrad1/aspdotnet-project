using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace OpenMarket.Models
{
    public class OrderDetail
    {
        public int Id { get; set; }
        public Order Order{ get; set; }
        public Product Product { get; set;}

        [Required]
        public decimal Price { get; set; }

        [Required]
        public int Quantity { get; set; }

        [NotMapped]
        public decimal CalcPrice { get {
            return Price*Quantity;
        }}
    }
}