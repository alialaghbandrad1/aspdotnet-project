using Microsoft.AspNetCore.Identity;

namespace OpenMarket.Models
{
    public class Review
    {
        public int Id {get; set;}
        public IdentityUser Reviewer {get; set;}
        public Product Product {get; set;}
        public int Rating {get; set;}
        public string Content {get; set;}
    }
}