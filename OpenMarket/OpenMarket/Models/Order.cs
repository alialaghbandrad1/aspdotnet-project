using System;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Identity;

namespace OpenMarket.Models
{
    public class Order
    {
        public int Id  { get; set; }
        public IdentityUser Buyer { get; set; }

        [Required, MinLength(0), MaxLength(100)]
        public string OrderShipName { get; set; }

        [Required, MinLength(0), MaxLength(100)]
        public string OrderAddress { get; set; }

        [Required, MinLength(0), MaxLength(100)]
        public string OrderCity { get; set; }

        [Required, MinLength(0), MaxLength(20)]
        public string OrderProvince { get; set; }

        [Required, MinLength(0), MaxLength(10)]
        public string OrderPostalCode { get; set; }

        [Required, MinLength(0), MaxLength(100)]
        public string OrderCountry { get; set; }

        [Required, MinLength(0), MaxLength(50)]
        public string OrderPhone { get; set; }

        [Required, MinLength(0), MaxLength(320)]
        public string OrderEmail { get; set; }

        // money below        
        [Required]
        public decimal OrderTotalPrice { get; set; }

        [Required]
        public decimal OrderShippingPrice { get; set; }

        [Required]
        public decimal OrderTaxes { get; set; } // tax for both total price and shipping

        [Required]
        public decimal OrderTotalPayable { get; set; } // order total + shipping + taxes

    }
}