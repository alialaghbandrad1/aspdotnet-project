using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using System.ComponentModel.DataAnnotations;
// added
using Microsoft.AspNetCore.Authorization;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Logging;
using OpenMarket.Data;

namespace OpenMarket.Pages
{
    [Authorize(Roles = "Admin, User")]
    public class ProfileModel : PageModel
    {   
        private readonly OpenMarket.Data.OpenMarketDbContext db;
        private UserManager<IdentityUser> _userManager;
        private SignInManager<IdentityUser> _signInManager;
        private readonly RoleManager<IdentityRole> roleManager;
        [BindProperty(SupportsGet = true)]
        public string Id { get; set; }

        [BindProperty]
        public string Email { get; set; }
        [BindProperty]
        public string Password { get; set; }
        [BindProperty]
        public IdentityUser Useri { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public ProfileModel(OpenMarketDbContext db, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            this.db = db;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = "Email")]
            public string Email { get; set; }

            [Required]
            [DataType(DataType.Password, ErrorMessage = "Current password required")]
            [Display(Name = "Current Password:")]
            public string CurrentPassword { get; set; }

            [Required]
            [Display(Name = "New Password:")]
            [StringLength(100, ErrorMessage = "The {0} must be at least {2} and max {1} characters long.", MinimumLength = 6)]
            [DataType(DataType.Password)]
            public string NewPassword { get; set; }
            [Required]
            [DataType(DataType.Password)]
            [Display(Name = "Confirm Password:")]
            [Compare("NewPassword", ErrorMessage = "New password and confirmation password do not match.")]
            public string NewPasswordConfirm { get; set; }
        }

        /// <summary>
        /// get user information
        /// </summary>
        /// <returns>no returns</returns>
        public async Task OnGet()
        {
            var user = await _userManager.GetUserAsync(User);
            Useri = user;

            Email = user.Email;
            // Password = user.PasswordHash;

        }




        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            var user = await _userManager.GetUserAsync(User);
            var result = await _userManager.ChangePasswordAsync(user, Input.CurrentPassword, Input.NewPassword);
            user.Email = Input.Email;
            user.UserName = Input.Email;
            user.NormalizedEmail = Input.Email.ToUpper();
            user.NormalizedUserName = Input.Email.ToUpper();
            await db.SaveChangesAsync();

            return RedirectToPage("/Index");
        }

    }
}
