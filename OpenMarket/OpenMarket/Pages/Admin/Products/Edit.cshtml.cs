using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using OpenMarket.Data;
using OpenMarket.Models;
// added
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Hosting;

namespace OpenMarket.Pages.Admin.Products
{
    [Authorize(Roles = "Admin")]
    public class EditModel : PageModel
    {
        private readonly OpenMarket.Data.OpenMarketDbContext _context;
        private IHostingEnvironment _environment;

        public EditModel(OpenMarket.Data.OpenMarketDbContext context, IHostingEnvironment environment)
        {
            _context = context;
            _environment = environment;
        }
        [BindProperty]
        public IFormFile UploadedFile { get; set; }
        [BindProperty]
        public Product Product { get; set; }
         [BindProperty]
        public int CatId { get; set; }
         [BindProperty]
        public ProductCategory NewCategory { get; set; }
        [BindProperty]
        public List<ProductCategory> ProductCategoryLM { get; set; } = new List<ProductCategory>();

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            ProductCategoryLM = _context.ProductCategories.ToList();
            if (id == null)
            {
                return NotFound();
            }

            Product = await _context.Products.FirstOrDefaultAsync(m => m.Id == id);
            if (Product == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            string imagePath = Product.PhotoPath;
            if(UploadedFile != null){
                string fileExtension = Path.GetExtension(UploadedFile.FileName).ToLower();
                string[] allowedExtentions = {".jpg", ".jpeg", ".gif", ".png"};
            if(!allowedExtentions.Contains(fileExtension)){
                ModelState.AddModelError(string.Empty, "Only image files (jpg, gif, png, jped) are allowed");
                return Page();
            }
            var invalids = System.IO.Path.GetInvalidFileNameChars();
            var newFileName = String.Join("_", UploadedFile.FileName.Split(invalids, StringSplitOptions.RemoveEmptyEntries)).Trim();
            var file = Path.Combine(_environment.ContentRootPath, "wwwroot", "Uploads", UploadedFile.FileName);
            try{
                using (var fileStream = new FileStream(file, FileMode.Create)){
                UploadedFile.CopyTo(fileStream);
            }
            }catch(Exception ex) when (ex is IOException || ex is SystemException){
                ModelState.AddModelError(string.Empty, "Internal error saving the uploaded file");
                return Page();
            }
            imagePath = Path.Combine("Uploads", newFileName);
            Product.PhotoPath = imagePath;
            }
            NewCategory = _context.ProductCategories.Find(CatId);
            Product.ProductCategory = NewCategory;
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Product).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(Product.Id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool ProductExists(int id)
        {
            return _context.Products.Any(e => e.Id == id);
        }
    }
}
