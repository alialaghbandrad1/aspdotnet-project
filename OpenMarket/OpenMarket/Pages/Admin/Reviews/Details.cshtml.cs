using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OpenMarket.Data;
using OpenMarket.Models;

using Microsoft.AspNetCore.Authorization;


namespace OpenMarket.Pages.Admin.Reviews
{
    [Authorize(Roles = "Admin")]

    public class DetailsModel : PageModel
    {
        private readonly OpenMarket.Data.OpenMarketDbContext _context;

        public DetailsModel(OpenMarket.Data.OpenMarketDbContext context)
        {
            _context = context;
        }

        public Review Review { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Review = await _context.Reviews.FirstOrDefaultAsync(m => m.Id == id);

            if (Review == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
