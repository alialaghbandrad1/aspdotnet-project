using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OpenMarket.Data;
using OpenMarket.Models;

namespace OpenMarket.Pages.Admin.Reviews
{
    public class IndexModel : PageModel
    {
        private readonly OpenMarket.Data.OpenMarketDbContext _context;

        public IndexModel(OpenMarket.Data.OpenMarketDbContext context)
        {
            _context = context;
        }

        public IList<Review> Review { get;set; }

        public async Task OnGetAsync()
        {
            Review = await _context.Reviews.Include(p => p.Product).ToListAsync();
            //Product = await _context.Products.Include(p => p.ProductCategory).FirstOrDefaultAsync(m => m.Id == id);
        }
    }
}
