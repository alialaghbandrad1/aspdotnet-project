using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using OpenMarket.Data;
using OpenMarket.Models;
// added
using Microsoft.AspNetCore.Authorization;
using Microsoft.Extensions.Logging;


namespace OpenMarket.Pages.Admin.Category
{
     [Authorize(Roles = "Admin")]
    public class DetailsModel : PageModel
    {
 
        private readonly OpenMarket.Data.OpenMarketDbContext _context;
        private readonly ILogger<DetailsModel> _logger;

        public DetailsModel(OpenMarket.Data.OpenMarketDbContext context, ILogger<DetailsModel> logger)
        {
            _context = context;
            
        }

        public ProductCategory ProductCategory { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            ProductCategory = await _context.ProductCategories.FirstOrDefaultAsync(m => m.Id == id);

            if (ProductCategory == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
