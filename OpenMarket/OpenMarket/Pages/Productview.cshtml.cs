using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OpenMarket.Data;
using OpenMarket.Models;

namespace OpenMarket.Pages
{
    public class ProductviewModel : PageModel
    {
        private readonly OpenMarketDbContext ctx;
        public List<Product> ProductsList { get; set; } = new List<Product>();
        private UserManager<IdentityUser> _userManager;
        public ProductviewModel(OpenMarketDbContext ctx, UserManager<IdentityUser> userManager){
            this.ctx = ctx;
            _userManager = userManager;
        }
        [BindProperty(SupportsGet = true)]
        public int Id {get; set;}
        [BindProperty]
        public int QuantityChosen {get; set;}
        [BindProperty]
        public Product SelectedProduct {get; set;}
        [BindProperty]
        public List<ProductCategory> CategoryList {get; set;} = new List<ProductCategory>();
        [BindProperty]
        public string CardBg {get; set;}
        [BindProperty]
        public List<Review> ReviewsList {get; set;} = new List<Review>();
        [BindProperty]
        public CartItem BoughtProduct {get; set;}
        [BindProperty]
        public List<IdentityUser> UserList {get; set;} = new List<IdentityUser>();
        public void OnGet()
        {
            ProductsList = ctx.Products.ToList();
            SelectedProduct = ctx.Products.Find(Id);
            CategoryList = ctx.ProductCategories.ToList();
            ReviewsList = ctx.Reviews.ToList();
            UserList = ctx.Users.ToList();
        }
        
        public IActionResult OnPost(){
            SelectedProduct = ctx.Products.Find(Id);
            //BoughtProduct = new CartItem { Buyer = user,  Product = SelectedProduct, Quantity = QuantityChosen };
            return RedirectToPage("/Checkout", new { ProductId = SelectedProduct.Id, ProductQuantity = QuantityChosen});
        }
    }
}
