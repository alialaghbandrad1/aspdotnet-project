using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.AspNetCore.Authorization;

using System.IO;
using OpenMarket.Data;
// using OpenMarket.Repositories;
// using OpenMarket.Services;
// using OpenMarket.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using OpenMarket.Models;

namespace OpenMarket.Pages
{
    [Authorize(Roles = "Admin")]
    public class ProductAddModel : PageModel
    {
        private readonly OpenMarketDbContext db;
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        private IHostingEnvironment _environment;
        // private IUploadRepository _uploadRepository;
        public ProductAddModel(OpenMarketDbContext db, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager,
        IHostingEnvironment environment) {
            this.db = db;
            this.userManager = userManager;
            this.signInManager = signInManager;
            _environment = environment;
        }

        [BindProperty]
        public IFormFile UploadedFile { get; set; }

        [BindProperty]
        public Product NewProduct { get; set; }
        [BindProperty]
        public int RefId { get; set; }
        [BindProperty]
        public List<ProductCategory> ProductCategoryLM { get; set; } = new List<ProductCategory>();
        [BindProperty]
        public List<int> CategoryIds { get; set; }
        public void OnGet()
        {
            ProductCategoryLM = db.ProductCategories.ToList();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            string imagePath = null;
            if(UploadedFile != null){
                string fileExtension = Path.GetExtension(UploadedFile.FileName).ToLower();
                string[] allowedExtentions = {".jpg", ".jpeg", ".gif", ".png"};
            if(!allowedExtentions.Contains(fileExtension)){
                ModelState.AddModelError(string.Empty, "Only image files (jpg, gif, png, jped) are allowed");
                return Page();
            }
            var invalids = System.IO.Path.GetInvalidFileNameChars();
            var newFileName = String.Join("_", UploadedFile.FileName.Split(invalids, StringSplitOptions.RemoveEmptyEntries)).Trim();
            var file = Path.Combine(_environment.ContentRootPath, "wwwroot", "Uploads", UploadedFile.FileName);
            try{
                using (var fileStream = new FileStream(file, FileMode.Create)){
                UploadedFile.CopyTo(fileStream);
            }
            }catch(Exception ex) when (ex is IOException || ex is SystemException){
                ModelState.AddModelError(string.Empty, "Internal error saving the uploaded file");
                return Page();
            }
            imagePath = Path.Combine("Uploads", newFileName);
            }
            NewProduct.ProductCategory = db.ProductCategories.Find(RefId);
            NewProduct.PhotoPath = imagePath;
            var user = User.Identity.Name;
            NewProduct.Seller = db.Users.FirstOrDefault(u => u.UserName == user);   
            NewProduct.CreatedOnDate = DateTime.Now;
            db.Products.Add(NewProduct);
            await db.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
