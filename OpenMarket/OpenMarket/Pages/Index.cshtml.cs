﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;

using OpenMarket.Data;
using OpenMarket.Models;
using Microsoft.EntityFrameworkCore;

namespace OpenMarket.Pages
{
    public class IndexModel : PageModel
    {
        private readonly OpenMarketDbContext db;
        public List<Product> ProductsList { get; set; } = new List<Product>();
        public List<ProductCategory> ProductCategories { get; set; } = new List<ProductCategory>();
        [BindProperty]
        public string PictureRef {get; set;}

        private readonly ILogger<IndexModel> _logger;

        public IndexModel(OpenMarketDbContext db, ILogger<IndexModel> logger)
        {
            this.db = db;
            _logger = logger;
        }

        public async Task OnGetAsync()
        {
            ProductsList = await db.Products.ToListAsync();
            ProductCategories = await db.ProductCategories.ToListAsync();
        }
    }
}
