using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OpenMarket.Data;
using OpenMarket.Models;

namespace OpenMarket.Pages
{
    public class CheckoutModel : PageModel
    {
        public readonly OpenMarketDbContext ctx;
        public List<Product> ProductsList { get; set; } = new List<Product>();
        public CheckoutModel(OpenMarketDbContext ctx){
            this.ctx = ctx;
        }
        [BindProperty (SupportsGet = true)]
        public Product BuyProduct {get; set;}

        [BindProperty (SupportsGet = true)]
        public int ProductId {get; set;}

        [BindProperty (SupportsGet = true)]
        public int ProductQuantity {get; set;}
        [BindProperty]
        public int PaymentDone {get; set;}
        [BindProperty]
        public decimal AddedPrice {get; set;}
        [BindProperty]
        public decimal TaxPrice {get; set;}
        [BindProperty]
        public decimal GrandTotal {get; set;}
        [BindProperty]
        public Order CurrOrder {get; set;}
        public void OnGet()
        {
            PaymentDone = 0;
            ProductsList = ctx.Products.ToList();
            BuyProduct = ctx.Products.Find(ProductId);
            if(BuyProduct.IsOnSale == true){
                AddedPrice = BuyProduct.DiscountedPrice * ProductQuantity;
            }else{
                AddedPrice = BuyProduct.Price * ProductQuantity;
            }
            //Item = ctx.Products.Find(BoughtItem.Product.Id);
            TaxPrice = AddedPrice * .15m;
            GrandTotal = AddedPrice + TaxPrice + 5.50m;
        }
        public IActionResult OnPost(){
            var user = User.Identity.Name;
            BuyProduct = ctx.Products.Find(ProductId);
            if(BuyProduct.IsOnSale == true){
                AddedPrice = BuyProduct.DiscountedPrice * ProductQuantity;
            }else{
                AddedPrice = BuyProduct.Price * ProductQuantity;
            }
            TaxPrice = AddedPrice * .15m;
            GrandTotal = AddedPrice + TaxPrice + 5.50m;
            CurrOrder.OrderShippingPrice = 5.50m;
            CurrOrder.OrderTotalPrice = AddedPrice;
            CurrOrder.OrderTotalPayable = GrandTotal;
            CurrOrder.OrderTaxes = TaxPrice;
            if(PaymentDone > 1){
                int NewQuantity = BuyProduct.QuantityAvailable - ProductQuantity;
                BuyProduct.QuantityAvailable = NewQuantity;
                OrderDetail CurrOrderDetail = new OrderDetail{ Order = CurrOrder, Product = BuyProduct, 
                Price = BuyProduct.IsOnSale == true ? BuyProduct.DiscountedPrice : BuyProduct.Price, Quantity = ProductQuantity };
                CurrOrder.Buyer = ctx.Users.FirstOrDefault(u => u.UserName == user); 
                if(CurrOrder.Buyer == null){
                    CurrOrder.Buyer = ctx.Users.FirstOrDefault(u => u.Email == "guest@guest.com"); 
                }
                ctx.Orders.Add(CurrOrder);
                ctx.OrderDetails.Add(CurrOrderDetail);
                ctx.SaveChanges();
                return RedirectToPage("/PaymentSuccess");
            }else{
                return Page();
            }
            
        }
    }
}
