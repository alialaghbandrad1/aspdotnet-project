using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using OpenMarket.Data;
using OpenMarket.Models;

namespace OpenMarket.Pages
{
    public class ProductsBrowseModel : PageModel
    {
        private readonly OpenMarketDbContext db;
        public List<Product> ProductsList { get; set; } = new List<Product>();
        public List<ProductCategory> ProductsCategories { get; set; } = new List<ProductCategory>();
        public ProductsBrowseModel(OpenMarketDbContext db){
            this.db = db;
        }
        [BindProperty(SupportsGet = true)]
        public int Id {get; set;}
        [BindProperty]
        public bool RecordFound {get; set;}
        [BindProperty]
        public ProductCategory SelectedCategory {get; set;}
        [BindProperty]
        public string CardBg {get; set;}
        public void OnGet()
        {
            RecordFound = false;
            SelectedCategory = db.ProductCategories.Find(Id);
            ProductsCategories = db.ProductCategories.ToList();
            ProductsList = db.Products.ToList();
        }
    }
}
