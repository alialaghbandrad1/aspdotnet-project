using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;

using Microsoft.AspNetCore.Authorization;

using System.IO;
using OpenMarket.Data;
// using OpenMarket.Repositories;
// using OpenMarket.Services;
// using OpenMarket.ViewModels;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc.Rendering;
using OpenMarket.Models;

namespace OpenMarket.Pages
{
    [Authorize]
    public class ProductAddModel : PageModel
    {
        private readonly OpenMarketDbContext db;
        private readonly UserManager<IdentityUser> userManager;
        private readonly SignInManager<IdentityUser> signInManager;
        public ProductAddModel(OpenMarketDbContext db, UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager) {
            this.db = db;
            this.userManager = userManager;
            this.signInManager = signInManager;
        }

        [BindProperty]
        public Product NewProduct { get; set; }

        public void OnGet()
        {
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            var user = User.Identity.Name;
            NewProduct.User = db.Users.FirstOrDefault(u => u.UserName == user);   
            NewProduct.CreatedOnDate = DateTime.Now;
            db.Products.Add(NewProduct);
            await db.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
