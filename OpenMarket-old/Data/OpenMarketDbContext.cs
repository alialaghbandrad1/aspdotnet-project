using OpenMarket.Models;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OpenMarket.Data
{
    public class OpenMarketDbContext : IdentityDbContext
    {
        public OpenMarketDbContext(DbContextOptions<OpenMarketDbContext> options) : base(options) { }
        public DbSet<Product> Products { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder) 
        {
            optionsBuilder.UseSqlite(@"Data source=OpenMarket.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder) 
        {
            // on event model creating
            base.OnModelCreating(modelBuilder);
            // modelBuilder.ApplyConfiguration(new BlogConfiguration());
            // HasNoKey();
        }
    }
}