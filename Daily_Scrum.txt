 ********* 2021/06/03 *********
** Ali **
1. Done / not done since last scrum:
	- Proposal: Mockup pages created & URL completed
	- Bitbucket created and shared
2. To do until next Scrum:
	- Start project with Index & Register & Login pages
3. Need assistance / figure things out
	-

** Diego **
1. Done / not done since last scrum:
- first scrum
2. To do until next Scrum:
- catching up with tutorials
- reading/researching up on asp.net cart
3. Need assistance / figure things out
- **tba**
** Leon **
1. Done / not done since last scrum:
- Created typical scenario in proposal
2. To do until next Scrum:
-  Research options for implementation cart
3. Need assistance / figure things out
-
** Malik **
1. Done / not done since last scrum:
- first scrum
2. To do until next Scrum:
- Research payment gateways in ASP, also do general research about razor projects
3. Need assistance / figure things out
-

********* 2021/06/04 *********
** Ali **
1. Done / not done since last scrum:
	- Proposal completed & submitted
        - Started project: Index & AddProduct pages in progress
2. To do until next Scrum:
	- AddProduct page
	- Index page: show products
3. Need assistance / figure things out
-
** Diego **
1. Done / not done since last scrum:
- Added checklist items for the user stories
2. To do until next Scrum:
- catching up with tutorials
- getting comfortable with register, login, logout
- researching cart implementation for vs code and asp.net
3. Need assistance / figure things out
- **tba**
** Leon **
1. Done / not done since last scrum:
<<<<<<< HEAD
	- Researched options for implementation cart
2. To do until next Scrum:
	- Continue to research options for implementation cart
=======
- 
2. To do until next Scrum:
- 
>>>>>>> f5d9cd338f58df490e3e599f72866af489e346ae
3. Need assistance / figure things out
-
** Malik **
1. Done / not done since last scrum:
- 
2. To do until next Scrum:
- 
3. Need assistance / figure things out
-

********* 2021/06/05 *********
** Ali **
1. Done / not done since last scrum:
	- User login
	- Create AddProduct page (in progress)
2. To do until next Scrum:
	- Continue AddProduct page
3. Need assistance / figure things out
-

** Diego **
1. Done / not done since last scrum:
- 
2. To do until next Scrum:
- practicing implementation of cart in bakery example
3. Need assistance / figure things out
-
** Leon **
1. Done / not done since last scrum:
-  try implementation of cart 
2. To do until next Scrum:
- try implementation of cart page
3. Need assistance / figure things out
-
** Malik **
1. Done / not done since last scrum:
- 
2. To do until next Scrum:
- 
3. Need assistance / figure things out
-

********* 2021/06/06 *********
** Ali **
1. Done / not done since last scrum:
- 
2. To do until next Scrum:
- 
3. Need assistance / figure things out
-
** Diego **
1. Done / not done since last scrum:
- 
2. To do until next Scrum:
- begun setting up the database tables
3. Need assistance / figure things out
- will need to discuss team about adjustments to db
	(enum, datetime not working as expected)
** Leon **
1. Done / not done since last scrum:
- 
2. To do until next Scrum:
- 
3. Need assistance / figure things out
-
** Malik **
1. Done / not done since last scrum:
- created small projects to practice ASP applications
2. To do until next Scrum:
- continue research, create test page in project
3. Need assistance / figure things out
-

********* 2021/06/07 *********
** Ali **
1. Done / not done since last scrum:
	- Working on add Product: description, quantity, created date
        - Studying Add Image to product
2. To do until next Scrum:
	- Add Image to product
3. Need assistance / figure things out
-

** Diego **
1. Done / not done since last scrum:
- 
2. To do until next Scrum:
- ** studying for midterm **
3. Need assistance / figure things out
-
** Leon **
1. Done / not done since last scrum:
-  try implement category from database
2. To do until next Scrum:
- continue research how create category 
3. Need assistance / figure things out
-
** Malik **
1. Done / not done since last scrum:
- researched ASP, created test page in project copy  
2. To do until next Scrum:
- start adding product search/display page
3. Need assistance / figure things out
-

********* 2021/06/08 *********
** Ali **
1. Done / not done since last scrum:
	- Working on add Product: description, quantity, created date
        - Studying Add Image to product
2. To do until next Scrum:
	-   Add Image to product
	-   Icollection for multiple images
	-   Add Admin and user roles (scaffolding?)
3. Need assistance / figure things out
	-

** Diego **
1. Done / not done since last scrum:
- 
2. To do until next Scrum:
- implementing entities to databse 
- practicing for cart
3. Need assistance / figure things out
-
** Leon **
1. Done / not done since last scrum:
- researched how create category
2. To do until next Scrum:
- try connected new tables to sqlite
3. Need assistance / figure things out
-
** Malik **
1. Done / not done since last scrum:
- solved issue with CSS and project not loading properly
2. To do until next Scrum:
- work on products page
3. Need assistance / figure things out
-

********* 2021/06/09 *********
** Ali **
1. Done / not done since last scrum:
	- Add Admin and user roles (scaffolding?)
2. To do until next Scrum:
	- Upload Image to product
	- Icollection for multiple images
3. Need assistance / figure things out
	-

** Diego **
1. Done / not done since last scrum:
- database implemented
- tables and entities adjusted with gregory
2. To do until next Scrum:
- cart implementation
3. Need assistance / figure things out
-
** Leon **
1. Done / not done since last scrum:
- implement page Add Product 

2. To do until next Scrum:
- Try add Repository in Category

3. Need assistance / figure things out
-
** Malik **
1. Done / not done since last scrum:
- created a product browse and view page
2. To do until next Scrum:
- continue working on products page
3. Need assistance / figure things out
-

********* 2021/06/10 *********
** Ali **
1. Done / not done since last scrum:
	- Participating in Upload image
	- Added Admin and user roles using scaffolding
2. To do until next Scrum:
	- Complete Upload image
	- 
3. Need assistance / figure things out
	-

** Diego **
1. Done / not done since last scrum:
- database and entities
2. To do until next Scrum:
- cart implementation
- adapting from example found
3. Need assistance / figure things out
- 
** Leon **
1. Done / not done since last scrum:
- implemen page New Category
2. To do until next Scrum:
- Create CRUD to User
3. Need assistance / figure things out
-
** Malik **
1. Done / not done since last scrum:
- refined product view and browse page, began working with picture
2. To do until next Scrum:
- add sale feature, continue adding features
3. Need assistance / figure things out
-

********* 2021/06/11 *********
** Ali **
1. Done / not done since last scrum:
	- Completed Upload image
2. To do until next Scrum:
	- Admin can CRUD users using scaffolding
3. Need assistance / figure things out
-
** Diego **
1. Done / not done since last scrum:
- 
2. To do until next Scrum:
- cart implementation
- adapting from example found
3. Need assistance / figure things out
- difficult to find for vs code strickly
** Leon **
1. Done / not done since last scrum:
- Implemented CRUD for Category
2. To do until next Scrum:
- Implement Edit/ Details for Category
3. Need assistance / figure things out
-
** Malik **
1. Done / not done since last scrum:
- added sale feature and other small improvements
2. To do until next Scrum:
- research payment methods
3. Need assistance / figure things out
-

********* 2021/06/12 *********
** Ali **
1. Done / not done since last scrum:
- 
2. To do until next Scrum:
- 
3. Need assistance / figure things out
-
** Diego **
1. Done / not done since last scrum:
- ** working cart in visual studio **
2. To do until next Scrum:
- Now to adapt cart to vs code!
3. Need assistance / figure things out
- ** tba **
** Leon **
1. Done / not done since last scrum:
- Implemented Edit/ Details for Category
2. To do until next Scrum:
- Implement Review Page
3. Need assistance / figure things out
-
** Malik **
1. Done / not done since last scrum:
- 
2. To do until next Scrum:
- 
3. Need assistance / figure things out
-

********* 2021/06/13 *********
** Ali **
1. Done / not done since last scrum:
	- User can Edit his/her Email & Password
2. To do until next Scrum:
	- Admin can CRUD Users
3. Need assistance / figure things out
-
** Diego **
1. Done / not done since last scrum:
- 
2. To do until next Scrum:
- 
3. Need assistance / figure things out
-
** Leon **
1. Done / not done since last scrum:
- Implemented Add New Review 
2. To do until next Scrum:
- Implement View all Reviews
3. Need assistance / figure things out
-
** Malik **
1. Done / not done since last scrum:
- fixed remaining issues in products section and tested basic payment
2. To do until next Scrum:
- proceed with creating payment using one item instead of cart
3. Need assistance / figure things out
-

********* 2021/06/14 *********
** Ali **
1. Done / not done since last scrum:
	- Admin can Edit and Delete Users
2. To do until next Scrum:
	- 
3. Need assistance / figure things out
-

** Diego **
1. Done / not done since last scrum:
- 
2. To do until next Scrum:
- 
3. Need assistance / figure things out
-
** Leon **
1. Done / not done since last scrum:
- 
2. To do until next Scrum:
- 
3. Need assistance / figure things out
-
** Malik **
1. Done / not done since last scrum:
- implemented correct payment, removed most bugs from application
2. To do until next Scrum:
- prepare for presentaion
3. Need assistance / figure things out
-